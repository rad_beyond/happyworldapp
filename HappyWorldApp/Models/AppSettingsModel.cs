﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyWorldApp.Models
{
    public class AppSettingsModel
    {
        public string Title { get; set; }
        public string GoogleApiKey { get; set; }
    }
}
