﻿/// <binding AfterBuild='@build' Clean='@clean' />
"use strict";

var gulp = require("gulp"),
    del = require("del"),
    minify = require("gulp-minify");

var paths = {
    webroot: "./wwwroot/",
    srcScripts: ['scripts/**/*.js', 'scripts/**/*.ts', 'scripts/**/*.map']
};
paths.jsDest = paths.webroot + paths.srcScripts[0];
paths.tsDest = paths.webroot + paths.srcScripts[1];
paths.jsMin = paths.webroot + "scripts/app-min.js";

var cleanTs = "ts:clean";
var cleanJs = "js:clean";
var compileTs = "ts:compile";
var minifyJs = "js:min";

// Important Tasks =============================================
gulp.task("@clean", [cleanJs]);
gulp.task("@build", [minifyJs]);
//==============================================================

gulp.task(cleanTs, function (cb) {
    return del(paths.tsDest, cb);
});

gulp.task(cleanJs, [cleanTs], function (cb) {
    //var excludeMin = "!" + paths.jsMin;
    //return del([paths.jsDest, excludeMin], cb);
    return del(paths.jsDest, cb)
});

gulp.task(compileTs, [cleanJs], function () {
    return gulp.src(paths.srcScripts)
        .pipe(gulp.dest('wwwroot/scripts'));
});

gulp.task(minifyJs, [compileTs], function () {
    var excludeMin = "!" + paths.jsMin;
    return gulp.src([paths.jsDest, excludeMin])
        .pipe(minify())
        .pipe(gulp.dest('wwwroot/scripts'));
});
