﻿
export interface IGoogleMap {
}

export class GoogleMap implements IGoogleMap {
    public static map: google.maps.Map;
    public static mapOptions: google.maps.MapOptions;
    public static mapContainer: HTMLElement;
}

function initMap() {
    let mapOptions: google.maps.MapOptions = {
        zoom: 8,
        center: { lat: -36.848461, lng: 174.763336 }
    };

    let mapContainer = document.getElementById("map-container");

    GoogleMap.map = new google.maps.Map(mapContainer, mapOptions);
    GoogleMap.mapOptions = mapOptions;
    GoogleMap.mapContainer = mapContainer;
}